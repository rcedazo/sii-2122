// Moneda.h: interface for the Moneda class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MONEDA_H__E2AECFA6_BFC9_498C_A5AF_71465008C3B1__INCLUDED_)
#define AFX_MONEDA_H__E2AECFA6_BFC9_498C_A5AF_71465008C3B1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef enum{EURO,DOLAR,LIBRA} tipoDinero;
typedef enum{UN_CENT,DOS_CENT,CINCO_CENT,DIEZ_CENT,VEINTE_CENT,CINCUENTA_CENT,UNO,DOS} tipoMoneda;

#define FACTOR_DINERO 100
class Dinero
{
	tipoDinero elTipoDinero;
	unsigned cantidad;
public:
	Dinero() {cantidad=0;elTipoDinero=EURO;}
	Dinero(unsigned valor)  {cantidad=valor;elTipoDinero=EURO;}
	unsigned getCantidad(){return cantidad;}
	tipoDinero getTipoDinero() {return elTipoDinero;}
	void operator+=(const Dinero masDinero)
	{if(this->elTipoDinero == masDinero.elTipoDinero) this->cantidad+=masDinero.cantidad;}
	void operator-=(const unsigned valor)
	{this->cantidad-=valor;}
	bool operator>=(const Dinero elPedido)
	{if(this->elTipoDinero == elPedido.elTipoDinero) return(this->cantidad>=elPedido.cantidad?true:false);}
	Dinero operator-(const Dinero elPedido)
	{if(this->elTipoDinero == elPedido.elTipoDinero) return(Dinero(this->cantidad-elPedido.cantidad));}
};

class Moneda: public Dinero  
{
	tipoMoneda elTipoMoneda;
public:
	Moneda(unsigned);
	virtual ~Moneda();
};

#endif // !defined(AFX_MONEDA_H__E2AECFA6_BFC9_498C_A5AF_71465008C3B1__INCLUDED_)
