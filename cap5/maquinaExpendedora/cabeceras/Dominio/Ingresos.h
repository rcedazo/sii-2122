// Ingresos.h: interface for the Ingresos class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INGRESOS_H__8D62D433_F843_4E4D_A2FB_B93A2236E45A__INCLUDED_)
#define AFX_INGRESOS_H__8D62D433_F843_4E4D_A2FB_B93A2236E45A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../comunes/Moneda.h"

class Ingresos
{
	Dinero elDineroIngresado;
public:
	Ingresos();
	virtual ~Ingresos();
	void anyadirMoneda(Dinero nuevaMoneda)
	{ elDineroIngresado += nuevaMoneda;}
	bool haySuficienteDinero(Dinero elPrecioPedido)
	{ return (elDineroIngresado>= elPrecioPedido ? true : false); }
	Dinero getDinero(){return elDineroIngresado;}

};

#endif // !defined(AFX_INGRESOS_H__8D62D433_F843_4E4D_A2FB_B93A2236E45A__INCLUDED_)
