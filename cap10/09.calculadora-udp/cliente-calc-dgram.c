#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>

int main(void)
{
  int sd;
  struct sockaddr_in server_addr, client_addr;
  struct hostent *hp;
  int num[2], res;

  sd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sd < 0){
    printf("Error en socket\n");
    return 1;
  }

  /* se obtiene y se rellena la dirección del servidor */
  bzero((char *)&server_addr, sizeof(server_addr));
  hp = gethostbyname ("127.0.0.1");
  if (hp == NULL){
    printf("Error en la llamada gethostbyname\n");
    return 1;
  }
  memcpy (&(server_addr.sin_addr), hp->h_addr, hp->h_length);
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(4200);

  printf ("Introduce primer numero\n");
  scanf("%d", &num[0]);

  printf ("Introduce segundo numero\n");
  scanf("%d", &num[1]);  

  num[0]=htonl(num[0]);
  num[1]=htonl(num[1]);

	/* los argumentos se convierten a formato de red */
	sendto(sd, (char *) num, 2 *sizeof(int), 0, (struct sockaddr *)&server_addr, sizeof(server_addr));

	/* envía la petición */
	recvfrom(sd, (char *)&res, sizeof(int), 0, NULL, NULL);

	/* recibe la respuesta */
	res = ntohl(res); /* se convierte el resultado al formato del computador */
	printf("Resultado es %d \n", res);
	close (sd);
return 0;
}


