// De: "Apuntes de Sistemas Inform�ticos Industriales" Carlos Platero.
// (R) 2013 Con licencia GPL.
// Ver permisos en licencia de GPL


#include "../includes/STDNombre.h"
#include "../includes/CNombre.h"
//#include "../includes/MFCNombre.h"

#include <iostream>



//M�todo �nico para producir los objetos nombres
INombre* INombre::factoriaObjetos(tipoTiraCaracteres tipo)
{
  if(tipo == ESTANDAR_STL) return new STDNombre;
  else if(tipo == ESTILO_C) return new CNombre;
  //else if(tipo == CADENA_MFC) return new MFCNombre;
  else return NULL;
}




int main ( void )
{
	INombre *pNombre1 = INombre::factoriaObjetos(ESTANDAR_STL);
	INombre *pNombre2 = INombre::factoriaObjetos(ESTILO_C);
	//INombre *pNombre3 = INombre::factoriaObjetos(CADENA_MFC);



	pNombre1->setNombre("Manolo Gonzalez");
	pNombre2->setNombre("Pedro Lopez");
	//pNombre3->setNombre("Ana Perez");

	std::cout << pNombre1->getNombre() << std::endl;
	std::cout << pNombre2->getNombre() << std::endl;
	//std::cout << pNombre3->getNombre() << std::endl;


	delete pNombre1, pNombre2;
	//delete pNombre3;

	return 0;

}
